package com.example;

/**
 * This is a class.
 */
public class Greeter {

  /**
   * This is a constructor.
   Hello this extra comment
   */
  public Greeter() {

  }

  //TODO: Add javadoc comment
  public String greet(String someone) {
    return String.format("Hello, %s!", someone);
  }
}
